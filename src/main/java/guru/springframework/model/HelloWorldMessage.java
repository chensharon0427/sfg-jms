package guru.springframework.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HelloWorldMessage implements Serializable {
    //copy from Serializable interface, and using context action to Randomly change.
    static final long serialVersionUID = 3224773608553078427L;
    //why Serializable? if want to send msg as an obj, apply it.
    //but if use json, do not ...need this Serializable
    private UUID id;
    private String message;
}
