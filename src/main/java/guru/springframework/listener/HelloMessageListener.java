package guru.springframework.listener;

import guru.springframework.config.JmsConfig;
import guru.springframework.model.HelloWorldMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;//《--------
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class HelloMessageListener {
    @JmsListener(destination= JmsConfig.MY_QUEUE)
    public void listen(@Payload HelloWorldMessage helloWorldMesage,
                       @Headers MessageHeaders headers, Message message
                       ){
        System.out.println("I got a message!!!");
        System.out.println(helloWorldMesage);
    }

    //dealing with return
    private final JmsTemplate jmsTemplate;

    @JmsListener(destination= JmsConfig.MY_SEND_RCV_QUEUE)
    public void listenForHello(@Payload HelloWorldMessage helloWorldMesage,
                       @Headers MessageHeaders headers, Message message
    ) throws JMSException {
        HelloWorldMessage payloadMsg= HelloWorldMessage
                .builder()
                .id(UUID.randomUUID())
                .message("world！")
                .build();
        jmsTemplate.convertAndSend(message.getJMSReplyTo(),payloadMsg);
    }
}
