package guru.springframework.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import guru.springframework.config.JmsConfig;
import guru.springframework.listener.HelloMessageListener;
import guru.springframework.model.HelloWorldMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class HelloSender {
    private final JmsTemplate jmsTemplate;
    private final ObjectMapper objectMapper;
    @Scheduled(fixedRate = 2000)
    public void sendMessage(){
        //System.out.println("sending a msg");
        HelloWorldMessage message= HelloWorldMessage
                .builder()
                .id(UUID.randomUUID())
                .message("hello world")
                .build();
        jmsTemplate.convertAndSend(JmsConfig.MY_QUEUE,message);
        //System.out.println("msg sent");
    }

    @Scheduled(fixedRate = 2000)
    public void sendAndReceiveMessage() throws JMSException {
        HelloWorldMessage message= HelloWorldMessage
                .builder()
                .id(UUID.randomUUID())
                .message("hello")
                .build();
        Message receivedMsg=jmsTemplate.sendAndReceive(JmsConfig.MY_SEND_RCV_QUEUE, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message helloMessage= null;
                try {
                    helloMessage = session.createTextMessage(objectMapper.writeValueAsString(message));
                    helloMessage.setStringProperty("_type","guru.springframework.sfgjms.model.HelloWorldMessage");
                    System.out.println("sending hello");
                    return helloMessage;
                } catch (JsonProcessingException e) {
                    //e.printStackTrace();
                    throw new JMSException("boom");
                }

            }
        });
       // System.out.println("msg sent");
        System.out.println(receivedMsg.getBody(String.class));//?????????
    }
}
